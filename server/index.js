
const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const Promise = require('bluebird')
const dbConfig = require('./config/database.conf')
var cors = require('cors')
const app = express()
mongoose.Promise = Promise

app.set('port', port)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // parse request application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({extended: true}))

  // parse request application/json
  app.use(bodyParser.json())

  // use cors for allow access on requests
  app.use(cors())
  require('./app/routes/users.routes')(app)
  require('./app/routes/article.routes')(app)
  require('./app/routes/technicalsheet.routes')(app)
  require('./app/routes/userthings.routes')(app)
  console.log('test')
  // Give nuxt middleware to express

  app.use(nuxt.render)
  mongoose.connect(dbConfig.url)
    .then(() => {
      console.log('Connected to the database')
    }).catch(err => {
      console.log('Cannot connect to the database. Exiting now...')
      process.exit()
    })
  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
