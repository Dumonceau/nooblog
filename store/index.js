import Vuex from 'vuex'

import mutations from './mutations'
import getters from './getters'
import actions from './actions'

const createStore = () => {
  return new Vuex.Store({
    state: {
      counter: 0,
      isConnected: false,
      first_name: '',
      last_name: '',
      pseudo: '',
      email: '',
      token: '',
      role: ''
    },
    mutations,
    actions,
    getters
  })
}

export default createStore
