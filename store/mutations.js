const mutations = {
  setIsConnected: (state, isConnected) => {
    state.isConnected = isConnected
  },
  setPseudo: (state, pseudo) => {
    state.pseudo = pseudo
  },
  setFirstName: (state, firstName) => {
    state.first_name = firstName
  },
  setLastName: (state, lastName) => {
    state.last_name = lastName
  },
  setEmail: (state, email) => {
    state.email = email
  },
  setToken: (state, token) => {
    state.token = token
  },
  setRole: (state, role) => {
      state.role = role
  }
}

export default mutations
