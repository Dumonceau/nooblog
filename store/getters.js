const getters = {
  getIsConnected: state => state.isConnected,
  getFirstName: state => state.first_name,
  getLastName: state => state.last_name,
  getPseudo: state => state.pseudo,
  getEmail: state => state.email,
  getToken: state => state.token,
  getRole: state => state.role
}
export default getters
